package de.bixilon.alpaka.quiz.data;

public enum QuizColors {
    RED,
    GREEN,
    BLUE,
    YELLOW
}
