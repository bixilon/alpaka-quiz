package de.bixilon.alpaka.quiz.data;

public enum QuizState {
    WAITING_FOR_CLIENTS,
    WAITING_FOR_ANSWERS,
    WAITING_FOR_NEXT_QUESTION,
    FINISHED
}
