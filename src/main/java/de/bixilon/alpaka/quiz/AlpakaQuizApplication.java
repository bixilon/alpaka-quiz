package de.bixilon.alpaka.quiz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlpakaQuizApplication {
    public static void main(String[] args) {
        SpringApplication.run(AlpakaQuizApplication.class, args);
    }
}
